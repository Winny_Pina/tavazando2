//================================================================================
//  @file         municipios.js
//  @version      0.0.1
//  @path         collections/
//  @description  Coleção para capturar os Municipios Brasileiros.
//  @author       Winny
//  @contact      http://www.swellitsolutions.com.br
//  @copyright    Copyright Swell It Solutions.
//================================================================================

Municipios = new Meteor.Collection("Tab_Municipio");