//================================================================================
//  @file         tipo_ocorrencia.js
//  @version      0.0.1
//  @path         collections/
//  @description  Coleção para capturar os Estados Brasileiros.
//  @author       Winny
//  @contact      http://www.swellitsolutions.com.br
//  @copyright    Copyright Swell It Solutions.
//================================================================================

TpOcorrencias = new Meteor.Collection("Tp_Ocorrencia", {
	idGeneration: 'MONGO'
});