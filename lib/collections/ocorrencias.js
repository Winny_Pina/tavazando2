//================================================================================
//  @file         ocorrencias.js
//  @version      0.0.1
//  @path         collections/
//  @description  Coleção para capturar as ocorrencias enviadas do app via WS para o BD.
//  @author       Winny
//  @contact      http://www.swellitsolutions.com.br
//  @copyright    Copyright Swell It Solutions.
//================================================================================

Ocorrencias = new Meteor.Collection("Tab_Ocorrencia", {
	idGeneration: 'MONGO'
});
/*
 * Status:
 * - 0 : Aberto
 * - 1 : Em Andamento
 * - 2 : Concluido
*/
Ocorrencias.allow({
	update: function(){ return !!Meteor.userId(); }
});


function toTwoDigits( number ){
	return number > 9 ? number.toString() : '0' + number;
}

Ocorrencias.helpers({
	getData: function(){
		if(!this.data) return "00/00/00 - 00:00:00";

		var year = this.data.getFullYear(),
				month = this.data.getMonth() + 1,
				day = this.data.getDay(),
				hour = this.data.getHours(),
				minutes = this.data.getMinutes(),
				seconds = this.data.getSeconds();
		
		return toTwoDigits(day) + '/' + toTwoDigits(month) + '/' + year + ' - ' 
				+ toTwoDigits(hour) + ':' + toTwoDigits(minutes) + ':' + toTwoDigits(seconds);
	},
	getTipo: function(){
		if( !this.tipo ) return "Indefinido";

		var text = TpOcorrencias.findOne({
			Tipo: this.tipo
		});

		return text ? text.Descricao: "Indefinido";
	},
	user: function(){
		if( this.userId ){
			try {
				return Usuarios.findOne( new Mongo.ObjectID( this.userId ) )
			} catch(e) {
				console.log(e);
			}
		}
	}
});