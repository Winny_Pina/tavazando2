//================================================================================
//  @file         router.js
//  @version      0.0.1
//  @path         lib/
//  @description  Parâmetros do pluguin iron:router que define as rotas do sistema.
//  @author       Winny
//  @contact      http://swellitsolutions.com.br/contato
//  @copyright    Copyright Swell It Solutions.
//================================================================================

Router.configure({
	layoutTemplate: 'layoutDefault'
});

Router.map(function(){

	this.route('forgotpassword', {
		path: '/forgotpassword',
		layoutTemplate: 'layoutLogin'
	});
	
	this.route('management', {
		path: '/management'
	});

	this.route('map', {
		path: '/map'
	});

	this.route('settings', {
		path: '/settings'
	});


	this.route('settings.add.email', {
		path: '/settings/addemail'
	});

	this.route('settings.change.password', {
		path: '/settings/changepassword'
	});

	this.route('charts', {
		path: '/charts'
	});


	/* Show notifications */
	this.route('notifications', {
		path: '/'
	});

	/*Login*/
	this.route('login', {
		path: '/login',
		template: 'signin',
		layoutTemplate: 'layoutLogin'
	});

	//Unless user is logged redirect to login
	this.onBeforeAction(function( context ){
		if( !Meteor.userId() && context.url != '/login')
			Router.go('login');
		else 
			this.next();
	});
	
	//Load Google Maps Api in map page
	this.onBeforeAction(function() {
		if( !GoogleMaps.loaded() )
			GoogleMaps.load();
		this.next();
	}, { only: ['map'] });
	
});

