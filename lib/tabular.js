function toTwoDigits( number ){
	return number > 9 ? number.toString() : '0' + number;
}

TabularOcorrencias = new Tabular.Table({
	name: 'Ocorrências',
	collection: Ocorrencias,
	columns: [
		{ data: 'getTipo()', title: 'Ocorrências' },
		{ data: 'data', title: 'Data Hora', render: function(date){
			
			/*if(!val) return "00/00/00 - 00:00:00";

			var year = val.getFullYear(),
					month = val.getMonth() + 1,
					day = val.getDay(),
					hour = val.getHours(),
					minutes = val.getMinutes(),
					seconds = val.getSeconds();
			
			return toTwoDigits(day) + '/' + toTwoDigits(month) + '/' + year + ' - ' 
					+ toTwoDigits(hour) + ':' + toTwoDigits(minutes) + ':' + toTwoDigits(seconds);
			*/
		
			da = new Date(date);
  			year = "" + da.getFullYear();
  			month = "" + (da.getMonth() + 1); if (month.length == 1) { month = "0" + month; }
			day = "" + da.getDate(); if (day.length == 1) { day = "0" + day; }
			hour = "" + da.getHours(); if (hour.length == 1) { hour = "0" + hour; }
			minute = "" + da.getMinutes(); if (minute.length == 1) { minute = "0" + minute; }
			second = "" + da.getSeconds(); if (second.length == 1) { second = "0" + second; }
			return day + "/" + month + "/" + year + " - " + hour + ":" + minute;

			}
		},
		{ data: '_id', visible: false },
		{ data: 'tipo', visible: false },
		{ data: 'userId', visible: false },
		{ data: 'descricao', visible: false },
		{ data: 'cep', visible: false },
		{ data: 'cidadeId', visible: false },
		{ data: 'local', visible: false },
		{ data: 'arquivos', visible: false },
		{ data: 'endereco', title: 'Endereço'},
		{ data: 'status', title: 'Status', tmpl: Meteor.isClient && Template.tabularStatus },
		{ tmpl: Meteor.isClient && Template.tabularActions },
		{ tmpl: Meteor.isClient && Template.bookCheckOutCell }
	],
	order: [[1, "desc"]],
	language: {
		sEmptyTable: 'Nenhum registro encontrado',
		sInfo: 'Mostrando de _START_ até _END_ de _TOTAL_ registros',
		sInfoEmpty: 'Mostrando 0 até 0 de 0 registros',
		sInfoFiltered: '(Filtrados de _MAX_ registros)',
		sInfoPostFix: '',
		sInfoThousands: '.',
		sLengthMenu: 'Mostrar _MENU_',
		sLoadingRecords: 'Carregando...',
		sProcessing: 'Processando...',
		sZeroRecords: 'Nenhum registro encontrado',
		sSearch: 'Pesquisar',
		oPaginate: {
			sNext: 'Próximo',
			sPrevious: 'Anterior',
			sFirst: 'Primeiro',
			sLast: 'Último'
		},
		oAria: {
			sSortAscending: ': Ordenar colunas de forma ascendente',
			sSortDescending: ': Ordenar colunas de forma descendente'
		}
	}
});