//================================================================================
//  @file         _signaturePublish.js
//  @version      0.0.1
//  @path         server/
//  @description    Cria as assinaturas publicas para os methods de CRUD.
//  @author      	Winny
//  @contact     	http://swellitsolutions.com.br/contato
//  @copyright   	Copyright Swell It Solutions.
//================================================================================

Meteor.publish('ocorrencias', function( userName ){
	/*var profile = Meteor.users.findOne( this.userId );
	// If this user exists
	if( profile ){
		var modifier = { fields: { NomeUF: 1 } };
		// The current user profile
		profile = profile.profile;
		// Is admin
		if( profile.type === 1 )
			return Ocorrencias.find();
		
		var filter = FiltroOperadoras.find({
			Nome_Empresa: profile.name
		}, modifier).fetch();

		filter = _.map( filter, function( filtroOcorrencia ){
			return { cidadeId: filtroOcorrencia.NomeUF };
		});
		// If has filter
		if( filter.length > 0 )
			return Ocorrencias.find({ $or: filter });
		else return [];
	} else return [];*/

	return Ocorrencias.find();
});

Meteor.publish('tipo.ocorrencias', function(){
	return TpOcorrencias.find();
});

Meteor.publish('filtro.operadoras', function( limit ){
	var profile = Meteor.users.findOne( this.userId );
	if( profile ){
		var modifier = { fields: { NomeUF: 1 } };
			profile = profile.profile;

		if( profile.type === 1 )
			return FiltroOperadoras.find({});
		else 
			return FiltroOperadoras.find({ Nome_Empresa: profile.name });
	} else return [];
});

Meteor.publish('usuarios', function( usuario ){
	return Usuarios.find();
});

/*
Meteor.publish('cities', function(){

    var profile = Meteor.users.findOne( this.userId );
	
	if( profile ){
		
		var modifier = { fields: { NomeUF: 1 } };
			profile = profile.profile,
			cities = [];
		
		_.each( FiltroOperadoras.find({ Nome_Empresa: profile.name }), function( doc ){
			if( cities.indexOf( doc.NomeUF ) == -1 )
				cities.push( doc.NomeUF );
		});

	} else return [];

});
*/