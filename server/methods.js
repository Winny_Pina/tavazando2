Meteor.methods({
	changeOcorrenciaStatus: function(ocorrenciaId, newStatus) {
		
		check(ocorrenciaId, String);
		check(newStatus, Number);
		// the check cannot  be made with > < because it shouuld be an integer
		if ( newStatus < 0 || newStatus > 2 ) {
			throw new Error("Invalid status");
		}

		ocorrenciaId = ocorrenciaId.substr(10,24);
		
		// Double negation to force the return of a boolean
		return !!Ocorrencias.upsert( new Mongo.ObjectID(ocorrenciaId), {
			$set: { status: newStatus }
		});
	},
	userAddEmail: function( email ){
		this.unblock();

		var user = Meteor.users.findOne(this.userId),
			userId = this.userId,
			error = null;

		_.each( user.emails, function( userEmail ){
			if( userEmail.address == email )
				error = 1;
		});

		if( !error ){
			Meteor.users.update(userId, {
				$push: {
					emails: {
						address: email,
						verified: true 
					}
				}
			}, function( err ){
				//if(!err)
				//	Accounts.sendVerificationEmail(userId, [email]);
			});

		}
	},
	countOcorrencias: function(city, year){

		var doeds = [0,0,0,0,0,0,0,0,0,0,0,0],
			finisheds = [0,0,0,0,0,0,0,0,0,0,0,0];

		_.each( Ocorrencias.find({
			cidadeId: city
		}).fetch(), function( ocrr ){
			var month = ocrr.data.getMonth();
			if( ocrr.data.getFullYear() == year ){
				doeds[month] += 1;
				if( ocrr.status && ocrr.status == 2 )
					finisheds[month] += 1;
			}
		});

		return [doeds, finisheds];
	}
});