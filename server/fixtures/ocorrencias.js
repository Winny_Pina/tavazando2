FixtureOcorrencia = [
	{
		"_id" : "55de137a3fd1c8710147f5fb",
		"tipo" : 1,
		"descricao" : "Vazamento de asfalto no meio da agua.",
		"endereco" : "Av. Missler 436",
		"cep" : "89140-000",
		"cidadeId" : "Blumenau",
		"local" : [
			-27.0136748,
			-49.5288116
		],
		"data" : new Date(),
		"__v" : 0,
		"arquivo" : "http://assets.lwsite.com.br/uploads/widget_image/image/360/36054/agua.jpg",
		"status": 0
	},
	{
		"_id" : "55de12dd3fd1c8710147f5fa",
		"tipo" : 4,
		"descricao" : "Vazamento de agua no meio do asfalto.",
		"endereco" : "Rua Principal XYZ",
		"cep" : "89.140-001",
		"cidadeId" : "São Paulo",
		"local" : [
			-49.4879879,
			-48.5487958
		],
		"data" : new Date(),
		"__v" : 0,
		"status": 0
	}
];