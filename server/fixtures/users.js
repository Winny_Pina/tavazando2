FixtureUser = [
	{
		username: 'CASAN',
		email: 'casan@casan.com.br',
		password: '123456',
		profile: {
			name: 'CASAN - Companhia Catarinense de Águas e Saneamento',
			type: 0
		}
	},
	{
		username: 'SANOVA',
		email: 'sanova@sanova.com.br',
		password: '123456',
		profile: {
			name: 'Sanova',
			type: 0
		}
	},
	{
		username: 'MASTER',
		email: 'master@master.com.br',
		password: '123456',
		profile: {
			name: 'Master',
			type: 1
		}
	}
];