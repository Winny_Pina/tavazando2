//================================================================================
//  @file        	app-entry.js
//  @version     	0.0.1
//  @path        	server/
//  @description  Ao iniciar a ãplicação, o servidor roda este arquivo para
//							 	monitorar e iniciar as tabelas não iniciadas.
//  @author      	Winny
//  @contact     	http://swellitsolutions.com.br/contato
//  @copyright   	Copyright Swell It Solutions.
//================================================================================
// When server start
Meteor.startup(function(){

	// Add fixtures to db if collections empty
	if( !TpOcorrencias.findOne() ){
		_.each( FixtureTipoOcorrencia, function( fixture ){
			TpOcorrencia.insert(fixture);
		});
	}

	if( !Ocorrencias.findOne() ) {
		_.each( FixtureOcorrencia, function(fixture){
			Ocorrencia.insert(fixture);
		});	
	}

	if( !Meteor.users.findOne() ){
		_.each( FixtureUser, function(fixture){
			Accounts.createUser(fixture);
		});
	}

	var smtp = {
		username: 'suporte@tavazando.com.br',
		password: 'R19seNHYxMTYYb6rLWbREg',
		server:'smtp.mandrillapp.com',
		port: 587
	};

	process.env.MAIL_URL = 'smtp://' + 
		encodeURIComponent(smtp.username) + ':' +
		encodeURIComponent(smtp.password) + '@' +
		encodeURIComponent(smtp.server) + ':' + smtp.port;

	// Log init
	console.log("####################### SERVER RUNNING #######################");

});