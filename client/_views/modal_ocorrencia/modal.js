Template.modalOcorrencia.helpers({
	
	currentOcorrencia: function(){
		
		var ocorrencia = Ocorrencias.findOne(
			Session.get('currentOcorrencia')
		);
		
		if( ocorrencia && 
			_.isArray( ocorrencia.arquivos ) &&
			ocorrencia.arquivos[0] != '0'
			){
			ocorrencia.arquivo = ocorrencia.arquivos[0];
		}

		return ocorrencia;
	},
	
});