function onClickMark( id ){
	return function(){
		Session.set('currentOcorrencia', id);	
		$('#modal-ocorrencia').openModal();
	}
}

var localMap = new ReactiveVar([-16.1112858,-50.1532228]);

Template.map.onCreated(function(){
	var self = this;

	self.autorun(function(){
		var sub1 = self.subscribe('tipo.ocorrencias'),
			sub2 = self.subscribe('ocorrencias');

		if( sub1.ready() && sub2.ready() ){	
			GoogleMaps.ready('occurrencesMap', function(map){
				_.each( Ocorrencias.find({
					$or: [
						{ status: 0 },
						{ status: undefined }
					]
				}).fetch(), function( ocorrencia ){
					
					var marker = new google.maps.Marker({
						position: {
							lat: ocorrencia.local[1],
							lng: ocorrencia.local[0]
						},
						animation: google.maps.Animation.DROP,
						map: map.instance,
						zoom: 5
					});

					localMap.set([ocorrencia.local[1], ocorrencia.local[0]]);
					marker.addListener('click', onClickMark( ocorrencia._id ));
				});
			});
		}
	});
});

Template.map.helpers({
	mapOptions: function(){
		if( GoogleMaps.loaded() ){
			var locals = localMap.get();
			return {
				center: new google.maps.LatLng( locals[0], locals[1] ),
				zoom: 4
			};
		}
	}
});