// //================================================================================
// //  @file         ev_signin.js
// //  @version      0.0.1
// //  @path         client/_controllers/events
// //  @description  EVENTS para Login.
// //  @author       Winny
// //  @contact      http://swellitsolutions.com.br/contato
// //  @copyright    Copyright Swell It Solutions.
// //================================================================================

Template.signin.events({
	'click #signin_button': function(event, template){
		
		event.preventDefault();

		var email = template.find('#email').value,
			password = template.find('#password').value;

		Meteor.loginWithPassword(email, password, function (err, user){
			if(err){
				console.log(err);
			} else {
				Session.set('search.filter', Meteor.user().profile.name);
				Router.go('/');
			}
		});
		
	},
	'click .forgot-password-trigger': function(){
		$('#forgot-password').openModal();
	}
});
