Template.settingsAddEmail.events({
	'click .add-email': function(event, template){
		event.preventDefault();
		var email = template.find('#email').value;
		
		if( email ){
			Meteor.call('userAddEmail', email, function( error ){
				//if( !error ) alert("Foi enviado um email de confirmação para esse email, abra o email e clique no link de confirmação!");
				if( !error ) alert("Email adicionado com sucesso!");
			});
		}
	}
});