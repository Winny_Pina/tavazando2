Template.settingsChangePassword.events({
	'click .change-password': function(event, template){
		event.preventDefault();

		var oldPassword = template.find('#old-password').value,
			newPassword = template.find('#new-password').value,
			newPassword2 = template.find('#new-password-2').value;

		if( newPassword2 == newPassword ){
			Accounts.changePassword(oldPassword, newPassword, function(err, content){
				if( !err )
					alert("Senha alterada com sucesso!");
			});
		} else {
			alert("As senhas não coincidem");
		}
	}
});