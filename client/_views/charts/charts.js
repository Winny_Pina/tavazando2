
var loadedSubs = new ReactiveVar(false),
	loadedData = new ReactiveVar(false),
	filterCity = new ReactiveVar(null),
	filterYear = new ReactiveVar( (new Date()).getFullYear() ),
	data = new ReactiveVar([[], []]);

Template.charts.onCreated(function(){
	//Code Here
	var instance = this;

	instance.autorun(function(){
		var operators = instance.subscribe('filtro.operadoras'),
			ocorrences = instance.subscribe('ocorrencias');

		if( operators.ready() && ocorrences.ready() )
			loadedSubs.set(true);
	});

	window.data = data;
});

Template.charts.helpers({
	cities: function (){
		return _.uniq( FiltroOperadoras.find().fetch(), false, function(item){
			return item.NomeUF;
		});
	},
	years: function(){
		var toReturn = [];
		for( var i = 2015, to = (new Date()).getFullYear(); i <= to; i++ )
			toReturn.push(i);

		return toReturn;
	},
	city: function(){
		return filterCity.get();
	},
	year: function(){
		return filterYear.get();
	},
	data: function(){
		if(! filterCity.get() )
			return [[],[]];

		return data.get();
	},
	loaded: function(){
		return loadedSubs.get();
	},
	loadedData: function(){
		return loadedData.get();
	}
});

var updateData = function(){
	loadedData.set(false);
	Meteor.call('countOcorrencias', filterCity.get(), filterYear.get(), function(error, result){
		if(!error) data.set(result);
		loadedData.set(true);
	});
}

Template.charts.events({
	'change #chart-filter-city': function( event ){
		var value = event.currentTarget.value;
		if( value ){
			filterCity.set(value);
		}
		updateData();
	},
	'change #chart-filter-date': function( event ){
		var value = event.currentTarget.value;
		if( value ) filterYear.set(value);
		updateData();
	}
});

