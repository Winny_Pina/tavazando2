Template.chart.onCreated(function(){
	
});

Template.chart.onRendered(function(){
	//console.log(this, arguments);
	
	if( _.isEmpty( this.data.data ) )
		this.data.data = [[],[]];

	if( _.isEmpty( this.data.width ) )
		this.data.width = 800;

	if( _.isEmpty( this.data.height ) )
		this.data.height = 400;

	if( !_.isEmpty( this.data.hidden ) )
		this.data.hidden = "display:none;";

	var context = this.firstNode.getContext('2d');
	
	this.data.chart = new Chart(context).Bar({
		labels: ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"],
		datasets: [
			{
				label: "Ocorrências feitas",
				fillColor: "rgba(220,220,220,0.5)",
				strokeColor: "rgba(220,220,220,0.8)",
				highlightFill: "rgba(220,220,220,0.75)",
				highlightStroke: "rgba(220,220,220,1)",
				data: this.data.data[0]
			},
			{
				label: "Ocorrências atendidas",
				fillColor: "rgba(151,187,205,0.5)",
				strokeColor: "rgba(151,187,205,0.8)",
				highlightFill: "rgba(151,187,205,0.75)",
				highlightStroke: "rgba(151,187,205,1)",
				data: this.data.data[1]
			}
		]
	}, {
		responsive: true
	});
});

//Template.chart.helpers({
	//width: 800,
	//height: 400,
	//hidden: ""
//});