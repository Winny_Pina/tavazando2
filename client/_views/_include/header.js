//================================================================================
//  @file         co_header.js
//  @version      0.0.1
//  @path         client/_controllers/
//  @description  CONTROLLER para Header/Navegação/Menu.
//  @author       Winny
//  @contact      http://swellitsolutions.com.br/contato
//  @copyright    Copyright Swell It Solutions.
//================================================================================

var toggleFullScreen = function() {
	
	if ((document.fullScreenElement && document.fullScreenElement !== null) ||
		(!document.mozFullScreen && !document.webkitIsFullScreen)) {
		if (document.documentElement.requestFullScreen) {
			document.documentElement.requestFullScreen();
		}
		else if (document.documentElement.mozRequestFullScreen) {
			document.documentElement.mozRequestFullScreen();
		}
		else if (document.documentElement.webkitRequestFullScreen) {
			document.documentElement.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
		}
	}
	else {
		if (document.cancelFullScreen) {
			document.cancelFullScreen();
		}
		else if (document.mozCancelFullScreen) {
			document.mozCancelFullScreen();
		}
		else if (document.webkitCancelFullScreen) {
			document.webkitCancelFullScreen();
		}
	}
}

Template.header.onRendered(function(){
	$('.menu-sidebar-collapse').sideNav({
			menuWidth: 180, // Default is 240
			edge: 'left', // Choose the horizontal origin
			closeOnClick: true // Closes side-nav on <a> clicks, useful for Angular/Meteor
		}
	);
	$('.dropdown-button').dropdown({
		constrain_width: false
	});
});

Template.header.events({
	'click .toggle-fullscreen': function() {
		toggleFullScreen();
	},
	'click .logout': function(){
		Meteor.logout();
		Router.go('login');
	}
});