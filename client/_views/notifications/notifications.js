Template.notifications.onCreated(function(){
	var self = this;

	self.autorun(function(){
		self.subscribe('tipo.ocorrencias');
		self.subscribe('filtro.operadoras');
		self.subscribe('usuarios');
	});
});

Template.notifications.helpers({
	options: function(){
		return TabularOcorrencias;
	},
	filter: function() {
		var user = Meteor.user();
		
		if( user && user.profile.type == 1 ) return {};

		var filter = _.map( FiltroOperadoras.find().fetch(), function( filtroOcorrencia ){
			
			if( user.profile && user.profile.name == "CASAN - Companhia Catarinense de Águas e Saneamento" ){
				/*if(
					filtroOcorrencia.NomeUF == 'Porto Belo' ||
					filtroOcorrencia.NomeUF == 'Bombinhas'  ||
					filtroOcorrencia.NomeUF == 'Ilhota'     ||
					filtroOcorrencia.NomeUF == 'Guabiruba'  ||
					filtroOcorrencia.NomeUF == 'Botuverá'   ||
					filtroOcorrencia.NomeUF == 'Porto Belo' ||
					filtroOcorrencia.NomeUF == 'Major Gercino' ||
					filtroOcorrencia.NomeUF == 'Biguaçu'    ||
					filtroOcorrencia.NomeUF == 'Angelina'   ||
					filtroOcorrencia.NomeUF == 'Antônio Carlos' ||
					filtroOcorrencia.NomeUF == 'Rancho queimado' ||
					filtroOcorrencia.NomeUF == 'São Bonifácio' ||
					filtroOcorrencia.NomeUF == 'Águas Mornas'  ||
					filtroOcorrencia.NomeUF == 'São Pedro de Alcântara' ||
					filtroOcorrencia.NomeUF == 'São José' ||
					filtroOcorrencia.NomeUF == 'Florianópolis'
				)
				return { cidadeId: filtroOcorrencia.NomeUF };*/
			} else return { cidadeId: filtroOcorrencia.NomeUF };
		});

		return filter.length == 0 ? {} : { $or: filter };
	}
});
