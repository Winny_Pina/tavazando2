//Template.tabularStatus.onRendered(function(){
	//$('select').material_select();
//});

Template.tabularStatus.helpers({
	equals: function(thing, other) {
		return thing === other;
	}
});

Template.tabularStatus.events({
	'change select': function( event, template ){
		var node = $(event.currentTarget),
			id = node.attr('data-id'),
			value = ~~node.val();

		console.log(id, value);

		Meteor.call('changeOcorrenciaStatus', id, value, function(){
			console.log(arguments);
		});
	}
});

function callModalEvent( id ){
	return function(){
		Session.set('currentOcorrencia', id);
		$('#modal-ocorrencia').openModal();
	};
}

Template.tabularActions.onRendered(function(){
	this.find('.modal-trigger')
		.addEventListener('click', callModalEvent( this.data._id ));
});


Template.tabularTipo.helpers({
	type: function( typeNumber ){
		var type = TpOcorrencia.findOne({ Tipo: typeNumber }).Descricao;
		//return type ? type.Descricao: null;
		return type;
	}
});
