Template.forgotPassword.events({
	'click .modal-action': function(event, template){
		var email = template.find('#email').value;
		
		console.log( email );

		Accounts.forgotPassword({
			email: email
		}, function(err, content){
			if( !err )
				alert("Email para redefinir senha enviado.");
			else if( err.error == 403 )
				alert("Email não cadastrado!");

			console.log(err, content);
		});
	}
});